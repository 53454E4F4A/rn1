/*---------------------------------------------------------
    -- typedef header
    --
    --@praktikum    RNP
    --@semester     WS13
    --@gruppe       3
    --@name         Schaeufler, Jonas   #2092427
    --              Bergmann  , Jonas   #2045479
    --@aufgabe      Aufgabe A1
    --@professor    Prof. Dr. Heitmann
---------------------------------------------------------*/

#ifndef _DEFINE_VS_TYPE
#define _DEFINE_VS_TYPE

/*
	image definition for buffer allocation
*/
#define IMAGE_WITH 320
#define IMAGE_HEIGHT 240
#define IMAGE_COLOR_CHANNELS 3
#define MAX_CLIENTS 4

/*
	opencv window settings
*/
#define WINDOW_TITLE "Simulator"

/*
	additional
*/	
#define POLL_TIMEOUT 10
#define IMAGE_SIZE IMAGE_WITH*IMAGE_HEIGHT*IMAGE_COLOR_CHANNELS
#define BUFFER_FRAME_COUNT 5
#define BUFFER_SIZE BUFFER_FRAME_COUNT*IMAGE_SIZE

/*
	name: video_stream_t
	description:	
	main data structure
	one instance
	allocated in main 
	passed on to all threads
	freed in main
*/
typedef struct image_buffer
{
    char *imageData;
	int imageSize;    
} image_buffer;

typedef struct video_stream_t 
{
	/* buffer */
	unsigned long nbytes;
	image_buffer image;		        // ImageData and ImageSize 
	pthread_mutex_t buffer_mutex;	// the mutex for above structure
	pthread_cond_t send_signal;  	// the signal on which the clients get notified if there is new data 
	/* client data */
	char *source_ip;	            // the ip address of the video source - first argument
	unsigned int source_port;	    // the port of the video source - second argument
	/* server data */
	unsigned int listen_port; 	    // the port on which we listen for incomming connections - third argument
	int client_count;
    int buf_widx;
} video_stream_t;

/*
	name: client_thread_args
	description:	
	one instance per connection to our server
	passed to the threads handling incomming connections
	allocated in server_thread		
	freed in client_handler	
*/

typedef struct client_thread_args 
{	
	video_stream_t *stream;		// pointer to the main data structure
	int client_socket;		    // file descriptor of the client socket
	int *client_count;    
} client_thread_args;

#endif

