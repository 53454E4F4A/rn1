#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#define NUM_THREADS 500000

struct thread_data{
   pthread_t thread_id;
   int sum;
   char *message;
   int* count;
   pthread_mutex_t* mutex;
};


#define CHECK_RC      if( rc != 0 ){perror( "" ); exit(1); }



struct thread_data thread_data_array[NUM_THREADS];


void *PrintHello(void *threadarg){
   struct thread_data *my_data;

   my_data = (struct thread_data *) threadarg;

   usleep(100000);

   pthread_mutex_lock( my_data->mutex );
   (*my_data->count)--;
   pthread_mutex_unlock( my_data->mutex );

   return NULL;
}




int main(){

  int i,j, rc;
  pthread_mutex_t mutex;
  int count = 0;

  rc=pthread_mutex_init( &mutex, NULL );


  pthread_attr_t tattr;
  rc=pthread_attr_init(&tattr);
  CHECK_RC;

  rc = pthread_attr_setdetachstate(&tattr, PTHREAD_CREATE_DETACHED);
  CHECK_RC;


  for( i=0;i<NUM_THREADS; i++){
     thread_data_array[i].thread_id = i;
     thread_data_array[i].sum = 0;
     thread_data_array[i].message = NULL;
     thread_data_array[i].mutex = &mutex;
     thread_data_array[i].count = &count;

     pthread_mutex_lock( &mutex );
     count++;
     pthread_mutex_unlock( &mutex );

     rc = pthread_create(&thread_data_array[i].thread_id, &tattr, PrintHello,
          (void *) &thread_data_array[i]);
     //CHECK_RC;
     if( rc != 0 ){
       perror( "" );
       printf( "Anzahl: %d\n", count );
       exit(1);
    }
     //j=i/100+10;
     //if( (i % 100 )==0 )usleep(10);
     if( count>800 )usleep(10);
     if( (i % 1000)==0 )printf( "Threads erzeugt: %5d, Aktive Threads: %5d\n", i, count );
  }

  return 0;
}
