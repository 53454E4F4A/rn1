/*---------------------------------------------------------
    -- server
    --
    --@praktikum    RNP
    --@semester     WS13
    --@gruppe       3
    --@name         Schaeufler, Jonas   #2092427
    --              Bergmann  , Jonas   #2045479
    --@aufgabe      Aufgabe A1
    --@professor    Prof. Dr. Heitmann
---------------------------------------------------------*/

#include "server.h"

/*
 *	entrypoint for the server
 *	listens on given port on incoming connections
 *	creates threads for handling the clients
 * 
 */

int server_thread(void *args)
{


 	pthread_t 	thread; 	        // handle to a new thread
	int 		listenfd = 0;	    // socket used for listening
	int 		clientfd = 0;	    // socket for accepting a client
	int retval = 0;
	struct sockaddr_in client_addr;	// address of our client
	struct sockaddr_in serv_addr;	// address structure of us
	socklen_t addr_len = sizeof(client_addr);
	video_stream_t *stream = args; 	// main data structure
	client_thread_args *p_cargs;	// argument to the clients
    int running = 1;		// :(
	int optval = 1;
	
	/* 
	 * 	create a socket and listen on it
	 */
	
	listenfd = socket(AF_INET, SOCK_STREAM,	IPPROTO_TCP);
	
	if(listenfd == -1) 
	{
        perror("creating socket");
	}
	
	memset(&serv_addr, '0', sizeof(serv_addr)); 
	
	retval = setsockopt( listenfd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval) );

	if(retval == -1) 
	{
	    perror("sock options");
	}
	
	serv_addr.sin_family = AF_INET;	
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);	                            // bind to every interface
	serv_addr.sin_port = htons(stream->listen_port);
	retval = bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr));   // bind the socket to ip:port

	if(retval == -1) 
	{
	    perror("bind");  
	}
	
	retval = listen(listenfd, 10); 				                                // start listening with a backlog of 10

	if(retval == -1) 
	{
	    perror("listen");
	}

	/*
	 * 	spawn a thread for every incoming connection
	 */	

	while(running)
	{

		clientfd = accept(listenfd, (struct sockaddr*)&client_addr, &addr_len);     // accept a connection on the listensocket 
		
		if(stream->client_count < MAX_CLIENTS) 
		{
		    printf("Accepted %d\n", stream->client_count);
		
		    if (clientfd == -1) 
		    {							                                            // store it into a new socket
			    perror("accept");
		    } 
		    else 
		    {
		        p_cargs = (client_thread_args*) malloc(sizeof(client_thread_args));	// allocate memory for the argument struct
			    p_cargs->stream = stream;						                    // main data structure
			    p_cargs->client_socket = clientfd;					                // the socket for this connection
			    p_cargs->client_count = &stream->client_count;

        		if (pthread_create(&thread, NULL,(void*) client_handler, (void*)p_cargs) != 0) 
        		{
        		    perror("creating thread");
    		    }
               		 
		        stream->client_count++;
        	}
		} 
		else 
		{
		    close(clientfd);
		}
	}
     close(listenfd);
     return EXIT_SUCCESS;
}

    /*
     * 	entrypoint for a thread handling a established connection
     */

void client_handler(void *args) {
    
    int running = 1;	                                        // :(
    int buf_idx = 0;  	                                        // current location in the buffer
    unsigned long nbytes = 0;
    unsigned long to_send = 0;
    fd_set working_set;
    struct timeval timeout;
    int rc = 0;
    timeout.tv_sec = 0;
    timeout.tv_usec = 10;
    char *sendbuf;
    client_thread_args *p_args = (client_thread_args*) args;	// arguments
    char *p_buf = p_args->stream->image.imageData;  
    int bytes_sent = p_args->stream->nbytes;	                // how many bytes we sent// pointer to the buffer
    struct pollfd out = {p_args->client_socket, POLLOUT, 0};	// return value

    sendbuf = malloc(BUFFER_SIZE);
    FD_SET(p_args->client_socket, &working_set);

    /*
    *  wait for the incoming data on a signal
    *  send what we have left in the buffer 
    */

    printf("locking mutex!\n");
 
    nbytes = p_args->stream->nbytes;
    while(running) 
    {   
        do 
        {
            pthread_mutex_lock(&p_args->stream->buffer_mutex);
	        printf("%d %d %lu\n", p_args->stream->buf_widx, buf_idx, nbytes);
	        to_send = p_args->stream->nbytes-nbytes;
	        
	        if(to_send > p_args->stream->image.imageSize)
	        {
	            to_send = p_args->stream->image.imageSize;
            }
	        
	        if(to_send > (p_args->stream->image.imageSize-buf_idx))
	        {
	            to_send = p_args->stream->image.imageSize-buf_idx;
            }
	        
	        if(p_args->stream->buf_widx >= buf_idx)
	        {
	            to_send = p_args->stream->buf_widx-buf_idx;
            }
	        
	        if(to_send == 0)
	        {
	            rc = pthread_cond_wait(&p_args->stream->send_signal, &p_args->stream->buffer_mutex); // wait on data and lock the mutex
	        }
	        else 
	        {
	            memcpy(sendbuf, p_buf+buf_idx, to_send);
	        }
	        
	        if(rc == -1)
	        {
	            perror("thread_cond_wait\n");
            };
            
	        pthread_mutex_unlock(&p_args->stream->buffer_mutex);
        } while(to_send == 0);

	    rc = poll(&out, 1, POLL_TIMEOUT);
	
	    if(rc == -1)
	    {
	        perror("poll");
        };

	    if(rc > 0 && out.revents & POLLOUT)             							    // check return 
        {
	        bytes_sent = send( p_args->client_socket, sendbuf, to_send, MSG_NOSIGNAL ); // send our data
	       
	        if(bytes_sent == -1) 
	        {
	            running = 0;	   
	            perror("send");
	        }

	        nbytes += bytes_sent;
	        buf_idx = buf_idx+bytes_sent;						                        // set our position in the buffer
	        buf_idx = buf_idx%p_args->stream->image.imageSize;				            // and wrap around if necessary
	    }
	  
        rc = 0;
        out.revents = 0;
    }
    
    (*(p_args->client_count))--;
    printf("dropped %d\n", *p_args->client_count);
    free(sendbuf);
    close(p_args->client_socket);
    free(args);
}

