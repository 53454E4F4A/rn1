/*---------------------------------------------------------
    -- server header
    --
    --@praktikum    RNP
    --@semester     WS13
    --@gruppe       3
    --@name         Schaeufler, Jonas   #2092427
    --              Bergmann  , Jonas   #2045479
    --@aufgabe      Aufgabe A1
    --@professor    Prof. Dr. Heitmann
---------------------------------------------------------*/

#ifndef _DFE_SERVER_H
#define _DEF_SERVER_H

#include "header.h"
#include <poll.h>

int server_thread(void *args);
void client_handler(void *args);

#endif

