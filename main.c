/*---------------------------------------------------------
    -- main
    --
    --@praktikum    RNP
    --@semester     WS13
    --@gruppe       3
    --@name         Schaeufler, Jonas   #2092427
    --              Bergmann  , Jonas   #2045479
    --@aufgabe      Aufgabe A1
    --@professor    Prof. Dr. Heitmann
---------------------------------------------------------*/

#include "header.h"
#include "client.h"
#include "server.h"

int main(int argc, char *argv[])
{
    pthread_t		client;		// handle for the client thread
    pthread_t 		server;		// handle for the server thread
    video_stream_t 	stream;		// main data structure
   
    if(argc != 4)
    {
        printf("\n Usage: %s <ip of server> <rport> <lport>\n", argv[0]);
        return EXIT_SUCCESS;
    } 
 
    /*	
    * 	fill main structure with arguments
    * 	1 - ip of the video source_ip
    * 	2 - port on which to connect on
    * 	3 - port on which to listen on
    */
    
    stream.nbytes = 0;
    stream.image.imageData = (char*) malloc(BUFFER_SIZE);
    stream.image.imageSize = BUFFER_SIZE;
    stream.source_ip = argv[1];
    stream.source_port = atoi(argv[2]);
    stream.listen_port = atoi(argv[3]);
    stream.client_count = 0;
    
    /*
    * initialise buffer, mutex & signal
    * default buffer size 230400 bytes    * 
    */
    
    pthread_mutex_init(&stream.buffer_mutex, NULL);	
    pthread_cond_init (&stream.send_signal, NULL);

    /*
     *	create threads and join the client 
     */

    pthread_create(&client, NULL, (void*)client_thread, (void*)&stream);
    pthread_create(&server, NULL, (void*)server_thread, (void*)&stream);   
    pthread_join(client, NULL);
    
    /*
     *	free stuff 
     */

    return EXIT_SUCCESS; 
}

