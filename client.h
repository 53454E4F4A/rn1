/*---------------------------------------------------------
    -- client header
    --
    --@praktikum    RNP
    --@semester     WS13
    --@gruppe       3
    --@name         Schaeufler, Jonas   #2092427
    --              Bergmann  , Jonas   #2045479
    --@aufgabe      Aufgabe A1
    --@professor    Prof. Dr. Heitmann
---------------------------------------------------------*/

#ifndef _DEF_CLIENT_H
#define _DEF_CLIENT_H

#include "header.h"

int client_thread(void *args);

#endif

