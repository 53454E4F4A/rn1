/*---------------------------------------------------------
    -- client
    --
    --@praktikum    RNP
    --@semester     WS13
    --@gruppe       3
    --@name         Schaeufler, Jonas   #2092427
    --              Bergmann  , Jonas   #2045479
    --@aufgabe      Aufgabe A1
    --@professor    Prof. Dr. Heitmann
---------------------------------------------------------*/

#include "client.h"

/*
 *	entrypoint of the producer	
 *	connects to given ip:port
 *	frame (or fragment) into the buffer
 *	signals clients of data arrival
 */

int client_thread(void *args)
{
    video_stream_t *stream = args;	                                            // main data structure
    int clientfd = 0;			                                                // socket
    int buf_idx = 0;			                                                // current position in the buffer
    int buf_server_idx = 0;
    int bytes_read = 0;			                                                // how many bytes read in the last call
    IplImage* 	image = 0;	
    struct sockaddr_in serv_addr;	                                            // server address
    image = cvCreateImage(cvSize(IMAGE_WITH,IMAGE_HEIGHT), IPL_DEPTH_8U, IMAGE_COLOR_CHANNELS);    
    cvNamedWindow(WINDOW_TITLE, CV_WINDOW_AUTOSIZE);                        	// create the OpenCV window to display the buffer
    
    /*
     *	creating a socket and connect on our given ip address and port
     */
    
    if((clientfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) 
    {
        perror("creating socket");
        return 1;
    }
    
    memset(&serv_addr, 0, sizeof(serv_addr));   
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(stream->source_port); 	                        // convert from host byte order to network byte order

    if(inet_pton(AF_INET, stream->source_ip, &serv_addr.sin_addr)<=0)	        //  id address from char[] to binary
    {
        perror("resolving address");
        return 1;
    } 

    if(connect(clientfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)	// connect on our socket;
    {
       perror("connecting");
       return 1;
    } 
    
    /*
     *	reading from the socket into our buffer
     *	notify the server and display the image 
     */
    
    while ( (bytes_read = read(clientfd, image->imageData+buf_idx, image->imageSize-buf_idx)) > 0)
    {
      
        if(bytes_read < 0) 
        {
            perror("reading"); return 1; 
        };
          
	    pthread_mutex_lock(&stream->buffer_mutex);		                                        // lock the mutex again
	    memcpy(stream->image.imageData+buf_server_idx, image->imageData+buf_idx, bytes_read);	
	    stream->nbytes += bytes_read;
	    pthread_cond_signal(&stream->send_signal); 		                                        // signal the server that data is available
	    stream->buf_widx = (buf_server_idx + bytes_read) % stream->image.imageSize;
	    pthread_mutex_unlock(&stream->buffer_mutex);	                                       	// unlock the mutex
	
	    cvShowImage(WINDOW_TITLE, image);		                        // show the image

        if ((cvWaitKey(5) & 255) == 27)
        { 
            break;		                	                            // check for keypress
        }
	
	    buf_server_idx += bytes_read;
	    buf_server_idx = buf_server_idx % stream->image.imageSize;
	    buf_idx += bytes_read;					                        // adjust buffer position
	    buf_idx = buf_idx % image->imageSize; 		                    // wrap around if necessary
    } 

    
    /* free */
    cvDestroyWindow( WINDOW_TITLE );	
    return EXIT_SUCCESS;
}

