#---------------------------------------------------------
    #- makefile
    #-
    #-@praktikum    RNP
    #-@semester     WS13
    #-@gruppe       3
    #-@name         Schaeufler, Jonas   #2092427
    #-              Bergmann  , Jonas   #2045479
    #-@aufgabe      Aufgabe A1
    #-@professor    Prof. Dr. Heitmann
#----------------------------------------------------------

CC      = gcc
CFLAGS  = -Wall
LDFLAGS = -lpthread -lm -l opencv_highgui -l opencv_core -l opencv_imgproc
BIN     = videostreamer
OBJ     = main.o client.o server.o

all: $(BIN)

%.o: %.c
	$(CC) $(CFLAGS) -c $<
	
videostreamer: $(OBJ)
	$(CC) $(CFLAGS) -o $(BIN) $(OBJ) $(LDFLAGS)
	
clean:
	rm -f $(BIN) *.o 
	
