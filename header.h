/*---------------------------------------------------------
    -- header
    --
    --@praktikum    RNP
    --@semester     WS13
    --@gruppe       3
    --@name         Schaeufler, Jonas   #2092427
    --              Bergmann  , Jonas   #2045479
    --@aufgabe      Aufgabe A1
    --@professor    Prof. Dr. Heitmann
---------------------------------------------------------*/

#ifndef _DEF_H_VS
#define _DEF_H_VS

#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h> 
#include <pthread.h>
#include <sys/select.h>
#include "opencv/cv.h"
#include "opencv/highgui.h"
#include "typedef.h"

#endif

